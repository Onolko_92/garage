/* eslint-disable no-param-reassign */
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require('path');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const withTM = require('next-transpile-modules')(['swiper', 'dom7']);

module.exports = withTM({
  experimental: { modern: false },
  webpack: (config, { isServer, webpack }) => {
    const { alias } = config.resolve;
    config.resolve.alias = {
      ...alias,
      '@constants': path.join(__dirname, './src/constants'),
      '@components': path.join(__dirname, './src/components'),
      '@public': path.join(__dirname, './public'),
      '@utils': path.join(__dirname, './src/utils'),
    };

    const originalEntry = config.entry;
    config.entry = async () => {
      const entries = await originalEntry();
      const POLYFILL_PATH = './src/polyfills.js';
      const MAIN = 'main.js';

      if (entries[MAIN] && !entries[MAIN].includes(POLYFILL_PATH)) {
        entries[MAIN].unshift(POLYFILL_PATH);
      }

      return entries;
    };

    config.plugins.push(
      new webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: JSON.stringify(process.env.NODE_ENV),
          __WEB__: !isServer,
          __NODE__: isServer,
        },
        __WEB__: !isServer,
        __NODE__: isServer,
      }),
    );

    config.module.rules.push({
      test: /\.(css|scss)$/,
      include: [/node_modules\/swiper/],
      use: [
        'extracted-loader',
        MiniCssExtractPlugin.loader,
        {
          loader: 'css-loader',
          options: {
            modules: false,
          },
        },
        {
          loader: 'postcss-loader',
          options: {
            config: {
              path: __dirname,
              ctx: {
                cssnano: {
                  preset: 'default',
                },
              },
            },
          },
        },
      ],
    });

    config.module.rules.push(
      {
        test: /\.(css|scss)$/,
        exclude: [/node_modules\/swiper/],
        use: [
          'extracted-loader',
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1,
              modules: {
                localIdentName: '[name]_[local]--[hash:base64:10]',
              },
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              config: {
                path: __dirname,
              },
            },
          },
        ],
      },
      {
        test: /\.(png|jpe?g|gif)$/i,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name]_[hash].[ext]',
              publicPath: `/_next/static/images`,
              outputPath: 'static/images',
            },
          },
        ],
      },
    );

    config.module.rules.push({
      test: /\.test.js$/,
      loader: 'ignore-loader',
    });

    config.plugins.push(
      new MiniCssExtractPlugin({
        filename: 'static/css/[name].css',
        chunkFilename: 'static/css/[name].chunk.css',
        ignoreOrder: true,
      }),
    );

    config.optimization.minimizer.push(new OptimizeCSSAssetsPlugin({}));

    return config;
  },
  serverRuntimeConfig: {},
  publicRuntimeConfig: {},
});
