module.exports = {
  parser: 'postcss-scss',
  plugins: {
    'postcss-reporter': { clearMessages: true },
    'postcss-import': {},
    autoprefixer: {},
    'postcss-preset-env': {
      stage: 0,
    },
    'postcss-custom-media': {},
    'postcss-custom-properties': {
      preserve: false,
    },
    'postcss-calc': {},
    'postcss-apply': {},
    'postcss-mixins': {},
    'postcss-nested': {},
    'postcss-simple-vars': {},
    'postcss-discard-duplicates': {},
    'postcss-combine-duplicated-selectors': {
      removeDuplicatedProperties: true,
    },
  },
};
