export const commonMenu = [
  {
    title: 'Услуги',
    id: 'services',
  },
  {
    title: 'Мастера',
    id: 'masters',
  },
  {
    title: 'Акции',
    id: 'stock',
  },
];

export const headerMenu = [
  ...commonMenu,
  {
    title: 'Контакты',
    id: 'contact',
  },
];
