import DIMA from '@public/minsk/dima.jpg';
import LEHA from '@public/minsk/leha.jpg';
import KATIA from '@public/minsk/katia.jpg';
import ANDREW from '@public/minsk/andrew.jpg';

export default [
  {
    name: 'Леха',
    jobs: 'Барбер',
    link: 'https://n338548.yclients.com/company:324492?o=m984964',
    img: LEHA,
  },
  {
    name: 'Тима',
    jobs: 'Барбер',
    link: 'https://n338548.yclients.com/company:324492?o=m1001167',
    img: DIMA,
  },
  {
    name: 'Катя',
    jobs: 'Барбер',
    link: 'https://n338548.yclients.com/company:324492?o=m1041953',
    img: KATIA,
  },
  {
    name: 'Андрей',
    jobs: 'Барбер',
    link: 'https://n338548.yclients.com/company:324492?o=m1168125',
    img: ANDREW,
  },
];
