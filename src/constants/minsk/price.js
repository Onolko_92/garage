export default [
  {
    title: 'Стрижка бороды и усов',
    price: '25 BYN',
  },
  {
    title: 'Мужская стрижка',
    price: '30 BYN',
  },
  {
    title: 'Детская стрижка',
    price: '25 BYN',
  },
  {
    title: 'Стрижки папа + сын',
    price: '50 BYN',
  },
  {
    title: 'Стрижка Bro + Bro',
    price: '55 BYN',
  },
];
