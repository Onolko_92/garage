import OG_IMAGE from '@public/minsk/backgroundMin1.png';

export default {
  title: 'Барбершоп Garage (Гараж) в Минске',
  description:
    'Барбершоп «Garage» — это барбершоп в Минске с мужским характером, здесь для мужчин созданы идеальные условия: интерьер выполнен под стиль гаража с обилием деталей. В барбершопе работают первоклассные мастера! Приходи в Барбершоп Garage (Гараж) в Минске',
  canonical: 'http://garagebarbershop.by/minsk',
  image: OG_IMAGE,
};
