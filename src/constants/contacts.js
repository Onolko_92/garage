import VK_IMAGE from '@public/vk.png';
import FACEBOOK_IMAGE from '@public/f.png';
import INSTAGRAM_IMAGE from '@public/ins.png';

import MOGILEV0 from '@public/mogilev/image0.jpg';
import MOGILEV1 from '@public/mogilev/image1.jpg';
import MOGILEV2 from '@public/mogilev/image2.jpg';
import MOGILEV3 from '@public/mogilev/image3.jpg';
import MOGILEV4 from '@public/mogilev/image4.jpg';
import MOGILEV5 from '@public/mogilev/image5.jpg';
import MOGILEV6 from '@public/mogilev/image6.jpg';
import MOGILEV7 from '@public/mogilev/image7.jpg';

import BackgroundMog1 from '@public/mogilev/backgroundMog1.jpg';
import mainBackgroundMog from '@public/mogilev/mainBackground.jpg';

import MINSK0 from '@public/minsk/image0.png';
import MINSK1 from '@public/minsk/image1.png';
import MINSK2 from '@public/minsk/image2.png';
import MINSK3 from '@public/minsk/image3.jpg';
import MINSK4 from '@public/minsk/image4.jpg';
import MINSK5 from '@public/minsk/image5.jpg';
import MINSK6 from '@public/minsk/image6.jpg';
import MINSK7 from '@public/minsk/image7.jpg';

import BackgroundMin1 from '@public/minsk/backgroundMin1.png';
import mainBackgroundMin from '@public/minsk/mainBackground.png';

import * as MOGILEV from '@constants/mogilev';
import * as MINSK from '@constants/minsk';

import CommonBackground from '@public/commonBackground3.jpg';
import minskBackgroundMasters from '@public/minsk/backgroundMasters.jpg';

import servicesMog1 from '@public/mogilev/servicesMogilev1.jpg';
import servicesMog2 from '@public/mogilev/servicesMogilev2.jpg';

import servicesMinsk1 from '@public/minsk/servicesMinsk1.jpg';
import servicesMinsk2 from '@public/minsk/servicesMinsk2.jpg';

const urlInstagramMogilev =
  'https://www.instagram.com/garage_mogilev/?igshid=yl15a0kkeqdl';
const urlInstagramMinsk =
  'https://www.instagram.com/garage_ok16/?igshid=5eqiu63agfjn';

export const coordinatesMOG = [53.902135, 30.337886];
export const coordinatesMIN = [53.8899762, 27.5736954];

export const mogilev = {
  coordinates: coordinatesMOG,
  phone: '+375 (29) 377-83-83',
  email: 'info@garagebarbershop.by',
  address: 'Первомайская улица, 31',
  city: 'Могилёв',
  hint: 'Могилеве',
  open: '10:00 – 21:00',
  openW: 'с 10:00 до 21:00',
  id: '1',
  follow: 'https://n131133.yclients.com/',
  imageBlock: [
    MOGILEV0,
    MOGILEV1,
    MOGILEV2,
    MOGILEV3,
    MOGILEV4,
    MOGILEV5,
    MOGILEV6,
    MOGILEV7,
  ],
  instagram: urlInstagramMogilev,
  masters: MOGILEV.masters,
  sociable: [
    {
      url: urlInstagramMogilev,
      logo: INSTAGRAM_IMAGE,
      name: 'instagram',
    },
    {
      url: 'https://vk.com/garagebarbershop_by',
      logo: VK_IMAGE,
      name: 'vk',
    },
  ],
  price: MOGILEV.price,
  stock: MOGILEV.stock,
  meta: MOGILEV.meta,
  background1: BackgroundMog1,
  mainBackground: mainBackgroundMog,
  mastersBackground: CommonBackground,
  mastersText:
    'В барбершопе работают первоклассные мастера, которые могут не \n только виртуозно использовать ножницы и бритву, но также и \n проконсультировать Вас в области стрижки или косметологии.',
  infoBlockTitle: 'Барбершоп «Garage» — это заведение с мужским характером.',
  infoBlockText:
    'Мы создали идеальные условия для мужчин. Особую атмосферу для того, \n чтобы расслабиться, создает музыка из виниловых пластинок. Кроме того, \n здесь есть все, что любит сильный пол: напитки в свободном доступе, \n возможность поиграть в Playstation или дартс. Хотите поговорить по \n душам с мастером и просто отдохнуть — команда барбершопа с радостью \n Вам в этом поможет.',
  servicesPrice: [servicesMog1, servicesMog2],
  additionally3:
    'Можно поиграть в Playstation, если пришёл раньше и мастер ещё занят с клиентом.',
  mainBlock: [
    'Первый атмосферный барбершоп с мужским \n характером в Могилеве',
    'Мужские стрижки и бритьё / Playstation / \n Напитки / Заруливай!',
    'У нас можно не бояться за свою стрижку. \n Работают профессионалы!',
  ],
};

export const minsk = {
  coordinates: coordinatesMIN,
  phone: '+375 (29) 377-83-82',
  email: 'info@garagebarbershop.by',
  address: 'Октябрьская улица, 16/4',
  city: 'Минск',
  open: '12:00 – 23:00',
  openW: 'с 12:00 до 23:00',
  hint: 'Минске',
  follow: 'https://n338548.yclients.com/',
  imageBlock: [
    MINSK0,
    MINSK3,
    MINSK1,
    MINSK6,
    MINSK2,
    MINSK4,
    MINSK5,
    MINSK6,
    MINSK7,
  ],
  instagram: urlInstagramMinsk,
  masters: MINSK.masters, // fix it is for mogilev
  id: '2',
  sociable: [
    {
      url: urlInstagramMinsk,
      logo: INSTAGRAM_IMAGE,
      name: 'instagram',
    },
    {
      url: 'https://vk.com/garage_ok16',
      logo: VK_IMAGE,
      name: 'vk',
    },
    {
      url:
        'https://facebook.com/Garage-Barbershop-Minsk-110816067249484/?ref=bookmarks',
      logo: FACEBOOK_IMAGE,
      name: 'facebook',
    },
  ],
  price: MINSK.price,
  stock: MINSK.stock,
  meta: MINSK.meta,
  background1: BackgroundMin1,
  mainBackground: mainBackgroundMin,
  mastersBackground: minskBackgroundMasters,
  mastersText:
    '«Garage» — дружная команда, которая ценит свою историю, \n сохраняет самобытность и по-настоящему любит свое дело. \n Трудолюбие, внимательность и профессионализм — главные приоритеты в работе барберов. \n Они талантливо владеют ножницами, триммером, опасной бритвой и \n предоставляют своим гостям услуги и сервис высокого качества.',
  masterSecondText:
    'В процессе работы мастера используют профессиональные материалы и \n оборудование, а также косметические средства от известных производителей.',
  infoBlockTitle: 'Атмосфера',
  infoBlockText:
    '«Garage» — поистине мужское пространство с особым характером. \n Это место, куда приходят за профессиональным уходом и \n возвращаются за возможностью расслабиться в компании единомышленников. \n Стильный интерьер, приятная музыка, брутальная атмосфера — созданные условия позволят отвлечься от повседневности и \n хорошо провести время.',
  servicesPrice: [servicesMinsk1, servicesMinsk2],
  additionally3: 'Свободная зона Wi-Fi, TV и всегда свежая пресса.',
  mainBlock: [
    '«Garage» — это не просто стильные стрижки и \n профессиональный сервис обслуживания.',
    'Это барбершоп с неформальной атмосферой, \n в котором каждому мужчине будет интересно.',
    'Опытные мастера, брутальное бритье, новейшие техники — \n здесь каждый сможет найти себя и обрести неповторимый стиль.',
  ],
};
