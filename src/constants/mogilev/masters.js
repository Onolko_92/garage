import ARTUR from '@public/mogilev/artur.jpg';
import DIMA from '@public/mogilev/dima.jpg';
import VLAD from '@public/mogilev/vlad.jpg';
import VITA from '@public/mogilev/vita.jpg';

export default [
  {
    name: 'Влад',
    jobs: 'Барбер',
    link: 'https://n131133.yclients.com/company:140170?o=m1158784',
    img: VLAD,
  },
  {
    name: 'Вита',
    jobs: 'Барбер',
    link: 'https://n131133.yclients.com/company:140170?o=m1070142',
    img: VITA,
  },
  {
    name: 'Дима',
    jobs: 'Барбер',
    link: 'https://n131133.yclients.com/company:140170?o=m625034',
    img: DIMA,
  },
  {
    name: 'Артур',
    jobs: 'Барбер',
    link: 'https://n131133.yclients.com/company:140170?o=m1116787',
    img: ARTUR,
  },
];
