import OG_IMAGE from '@public/mogilev/ogMogilev.jpg';

export default {
  title: 'Барбершоп Garage (Гараж) в Могилеве',
  description:
    'Барбершоп «Garage» — это барбершоп в Могилеве с мужским характером, здесь для мужчин созданы идеальные условия: интерьер выполнен под стиль гаража с обилием деталей. В барбершопе работают первоклассные мастера! Приходи в Барбершоп Garage (Гараж) в Могилеве ',
  canonical: 'http://garagebarbershop.by',
  image: OG_IMAGE,
};
