export default [
  {
    title: 'Стрижка бороды и усов',
    price: '15 BYN',
  },
  {
    title: 'Мужская стрижка',
    price: '20 BYN',
  },
  {
    title: 'Детская стрижка',
    price: '15 BYN',
  },
  {
    title: 'Стрижки папа + сын',
    price: '30 BYN',
  },
  {
    title: 'Стрижка Bro + Bro',
    price: '35 BYN',
  },
];
