/* eslint-disable global-require */
if (typeof window !== 'undefined') {
  require('core-js/es/array');
  require('core-js/es/object');
  require('core-js/es/map');
  require('core-js/es/set');
  require('core-js/es/array/includes');
  require('core-js/es/object/values');
  require('core-js/es/string/includes');
  require('core-js/es/string/starts-with');
  require('core-js/es/string/ends-with');
  require('core-js/es/symbol/iterator');
  require('core-js/es/symbol');
  require('core-js/es/promise');
}
