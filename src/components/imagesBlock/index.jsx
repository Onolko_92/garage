// @flow
import React, { memo } from 'react';
// $flow-disable-line
import Slider from 'react-id-swiper';

import style from './style.scss';

type Props = {
  page: Object,
};

export default memo<Props>(({ page }: Props) => {
  const isClient = typeof window !== 'undefined';
  const isMobile = isClient && window.innerWidth < 768;
  const params = {
    containerClass: 'swiper-container',
    slidesPerView: !isMobile ? 'auto' : '1',
    spaceBetween: 16,
    slideClass: style.slide_wrap,
    loop: true,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    autoplay: {
      delay: 2500,
      disableOnInteraction: false,
    },
  };

  const commonData = {
    backgroundPosition: 'center center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
  };

  const cards = page.imageBlock.map(i => {
    return (
      <div
        style={{
          backgroundImage: `url(${i})`,
          ...commonData,
        }}
        key={i}
        className="lazy"
      />
    );
  });

  return (
    <div className={style.image_block}>
      <Slider {...params}>{cards}</Slider>
    </div>
  );
});
