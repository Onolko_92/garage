/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable react/jsx-key */
// @flow
import React, { memo } from 'react';
// $flow-disable-line
import smoothScrollIntoView from 'smooth-scroll-into-view-if-needed';

import { commonMenu } from '@constants/menu';
import Logo from '@public/logo.png';

import lang from './translations';
import style from './style.scss';

type Props = {
  page: Object,
};

const getSociableLinks = sociable => {
  return sociable.map(({ logo, name, url }) => (
    <div key={name} className={style.sociable_item}>
      <a target="_blank" rel="noopener noreferrer" href={url}>
        <img data-src={logo} alt={name} />
      </a>
    </div>
  ));
};

const onClick = (e, id) => {
  e.preventDefault();
  smoothScrollIntoView(document?.getElementById(id), {
    behavior: 'smooth',
  });
};

const getMenu = () =>
  commonMenu.map(m => (
    <a
      key={m.title}
      className={style.menu_link}
      onClick={e => onClick(e, m.id)}
    >
      {m.title}
    </a>
  ));

export default memo<Props>(({ page }: Props) => {
  const { sociable } = page;
  const { links, following, info, rules } = lang.ru;
  return (
    <div className={style.footer}>
      <div className={style.footer__logo}>
        <img data-src={Logo} alt="Logo" />
        <span className={style.menu_link}>{info}</span>
        <span className={style.menu_link}>{rules}</span>
      </div>
      <div className={style.footer__contact_wrapper}>
        <div>
          <span className={style.titles}>{links}</span>
          <div className={style.footer__links}>{getMenu()}</div>
        </div>
        <div>
          <span className={style.titles}>{following}</span>
          <div className={style.footer__sociable}>
            {getSociableLinks(sociable)}
          </div>
        </div>
      </div>
      <div className={style.footer__extra}>
        <span className={style.hiden_block}>{info}</span>
        <span className={style.hiden_block}>{rules}</span>
      </div>
    </div>
  );
});
