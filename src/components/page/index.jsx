/* eslint-disable jsx-a11y/heading-has-content */
// @flow
import React, { memo } from 'react';
// $flow-disable-line
import cn from 'classnames';

import Info from '@components/info';
import Additionally from '@components/additionally';
import Contact from '@components/contact';
import Services from '@components/services';
import Stock from '@components/stock';
import ImagesBlock from '@components/imagesBlock';
import InstagramBlock from '@components/instagramBlock';
import MastersBlock from '@components/mastersBlock';
import Meta from '@components/metaTags';
import MainBlock from '@components/mainBlock';

import style from './style.scss';

type Props = {
  page: Object,
};

export default memo<Props>((props: Props) => {
  const { page } = props;
  return (
    <>
      <Meta {...props} />
      <MainBlock {...props} />
      <Info {...props} />
      <div className={style.second_block}>
        <div className={style.bg_wrap}>
          <div
            className={cn(style.background, 'lazy')}
            style={{
              background: `url(${page.background1}) center center no-repeat`,
            }}
          />
        </div>
        <div className={style.filter} />
        <h1>{page.infoBlockTitle}</h1>
        <div className={style.second_block__description}>
          {page.infoBlockText}
        </div>
      </div>
      <Additionally {...props} />
      <Services {...props} />
      <Stock {...props} />
      <ImagesBlock {...props} />
      <InstagramBlock {...props} />
      <MastersBlock {...props} />
      <Contact {...props} />
    </>
  );
});
