// @flow
import React, { memo } from 'react';
import SCISSORS from '@public/scissors.jpg';
import DRINKS from '@public/drinks.jpg';
import GAMES from '@public/games.jpg';

import style from './style.scss';

type Props = {
  page: Object,
};

export default memo<Props>(({ page }: Props) => {
  return (
    <div className={style.additionally}>
      <span className={style.additionally__title}>
        Мы не испортим прическу или бороду
      </span>
      <span className={style.additionally__text}>
        {'Тебя будет стричь мастер с большим \n опытом работы'}
      </span>
      <div className={style.additionally__wrapper}>
        <div className={style.additionally__wrapper__container}>
          <img data-src={SCISSORS} alt={page.address} />
          <span className={style.additionally__wrapper__container__title}>
            Чистые инструменты
          </span>
          <span className={style.additionally__wrapper__container__text}>
            Перед твоей стрижкой, мастер проведет полную стерилизацию всего
            оборудования.
          </span>
        </div>
        <div className={style.additionally__wrapper__container}>
          <img data-src={DRINKS} alt={page.address} />
          <span className={style.additionally__wrapper__container__title}>
            Бесплатныe напитки
          </span>
          <span className={style.additionally__wrapper__container__text}>
            В твоем распоряжении будет бар с напитками: кола, кофе, чай, все
            бесплатно.
          </span>
        </div>
        <div className={style.additionally__wrapper__container}>
          <img data-src={GAMES} alt={page.address} />
          <span className={style.additionally__wrapper__container__title}>
            Комфортная атмосфера
          </span>
          <span className={style.additionally__wrapper__container__text}>
            {page.additionally3}
          </span>
        </div>
      </div>
      {page.masterSecondText && (
        <span className={style.additionally__second_text}>
          {page.masterSecondText}
        </span>
      )}
    </div>
  );
});
