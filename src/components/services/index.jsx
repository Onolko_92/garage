// @flow
import React from 'react';
// $flow-disable-line
import Slider from 'react-id-swiper';

import commonBackground from '@public/background2.jpg';

import style from './style.scss';

type Props = {
  page: Object,
};

export default React.memo<Props>(({ page }: Props) => {
  const params = {
    slidesPerView: 'auto',
    spaceBetween: 16,
    loop: true,
    autoplay: {
      delay: 3000,
    },
  };
  return (
    <div className={style.third_block} id="services">
      <div className={style.bg_wrap}>
        <div
          className={style.background}
          style={{
            background: `url(${commonBackground}) center center no-repeat`,
          }}
        />
      </div>
      <div className={style.filter} />
      <div className={style.minsk_wrapper}>
        <Slider {...params}>
          {page.servicesPrice.map(i => {
            return <img key={i} data-src={i} alt={`img${i}`} />;
          })}
        </Slider>
      </div>
    </div>
  );
});
