/* eslint-disable jsx-a11y/heading-has-content */
// @flow
import React, { memo } from 'react';
// $flow-disable-line
import TypeIt from 'typeit-react';
// $flow-disable-line
import cn from 'classnames';

import Follow from '@components/common/followButton';
import overlay from '@public/overlay-pattern.png';

import style from './style.scss';

type Props = {
  page: Object,
};

export default memo<Props>(({ page }: Props) => {
  return (
    <div className={style.first_block}>
      <div className={style.bg_wrap}>
        <div
          className={cn(style.background, 'lazy')}
          style={{
            background: `url(${page.mainBackground}) center center no-repeat`,
          }}
        />
      </div>
      <div
        className={style.overlay}
        style={{ background: `url(${overlay})` }}
      />
      <div className={style.filter} />
      <h1 className={style.title}>{`Барбершоп Garage в ${page.hint}`}</h1>
      <div className={style.output} id="output">
        <TypeIt
          options={{
            speed: 100,
            cursor: false,
            lifeLike: true,
            loop: true,
          }}
          getBeforeInit={
            instance =>
              instance
                .type(page.mainBlock[0])
                .pause(2000)
                .delete(page.mainBlock[0].length)
                .pause(1000)
                .type(page.mainBlock[1])
                .pause(2000)
                .delete(page.mainBlock[1].length)
                .pause(1000)
                .type(page.mainBlock[2])
                .pause(2000)
                .delete(page.mainBlock[2].length)
            // $flow-disable-line
          }
        />
      </div>
      <Follow page={page} type="first" text="follow" />
    </div>
  );
});
