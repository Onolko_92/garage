// @flow
import React from 'react';

type Props = {
  scroll: number,
};

export default ({ scroll }: Props) => {
  const background = `linear-gradient(
      to right,
      rgb(240, 12, 26) ${scroll}%, 
      transparent  0)`;
  return (
    <div
      style={{
        position: 'fixed',
        background,
        width: '100%',
        height: '4px',
        zIndex: '3',
        transition: 'width .6s ease',
      }}
    />
  );
};
