// @flow
import React, { memo } from 'react';

import style from './style.scss';

type Props = {
  page: Object,
};

const text = 'Мы делимся с вами атмосферой \n нашего салона в ';

export default memo<Props>(({ page }: Props) => (
  <div className={style.instagram_block}>
    {text}
    <a href={page.instagram} target="_blank" rel="noopener noreferrer">
      Instagram
    </a>
  </div>
));
