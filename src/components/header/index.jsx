/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
// @flow
import React, { memo } from 'react';
// $flow-disable-line
import smoothScrollIntoView from 'smooth-scroll-into-view-if-needed';
// $flow-disable-line
import cn from 'classnames';
// $flow-disable-line
import Link from 'next/link';
// $flow-disable-line
import cookies from 'js-cookie';

import { minsk, mogilev } from '@constants/contacts';
import HEADER_LOGO from '@public/black_logo.jpg';
import { headerMenu } from '@constants/menu';

import Follow from '@components/common/followButton';

import style from './style.scss';

type Props = {
  setPage: Object => void,
  page: Object,
};

const onClick = (e, id) => {
  e.preventDefault();
  smoothScrollIntoView(document?.getElementById(id), {
    behavior: 'smooth',
  });
};

const getCityLink = (city, setPage, name, page) => {
  const activeClass = cn(style.link, {
    [style.active]: city.id === page.id,
  });

  const cV = city.id === '2' ? 'minsk' : 'mogilev';

  let href = '/';
  if (city.id === '2') {
    href = '/minsk';
  }
  const handleChange = () => {
    cookies.set('city', cV, {
      expires: 365,
      path: '/',
    });
    setPage(city);
  };
  return (
    <span className={activeClass} onClick={handleChange}>
      <Link href={href}>
        <a>{name}</a>
      </Link>
    </span>
  );
};

const getMenu = () =>
  headerMenu.map(m => (
    <a
      key={m.title}
      className={style.menu_link}
      onClick={e => onClick(e, m.id)}
    >
      {m.title}
    </a>
  ));

export default memo<Props>(({ setPage, page }: Props) => {
  return (
    <div className={style.header}>
      <div>
        <img
          className={style.header__logo}
          data-src={HEADER_LOGO}
          alt="header_logo"
        />
      </div>
      <div className={style.header__menu}>{getMenu()}</div>
      <div style={{ display: 'flex' }}>
        <div className={style.header__phone}>
          <a href={`tel:${page.phone}`}>{page.phone}</a>
        </div>
        <Follow page={page} type="header" text="follow" />
      </div>
      <div className={style.header__links}>
        {getCityLink(minsk, setPage, 'Минск', page)}
        {getCityLink(mogilev, setPage, 'Могилев', page)}
      </div>
    </div>
  );
});
