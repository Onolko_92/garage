// @flow
import React, { memo } from 'react';
import YMaps from './map';

import style from './style.scss';

type Props = {
  page: Object,
};

export default memo<Props>((props: Props) => {
  const { page } = props;
  return (
    <div className={style.contact__container} id="contact">
      <YMaps {...props} />
      <div className={style.contact__info}>
        <div>
          <span className={style.contact__info__title}>Контакты</span>
        </div>
        <div>
          <span className={style.contact__info__phone}>
            Телефон:
            <a href={`tel:${page.phone}`}>{page.phone}</a>
          </span>
        </div>
        <div>
          <span className={style.contact__info__common}>
            {`${page.city}, ${page.address}`}
          </span>
          <span className={style.contact__info__common}>
            {`Мы открыты: ${page.open}`}
          </span>
          <span className={style.contact__info__common}>Без выходных</span>
        </div>
      </div>
    </div>
  );
});
