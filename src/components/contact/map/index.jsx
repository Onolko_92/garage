/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-hooks/rules-of-hooks */
// @flow
import React, { memo, useRef } from 'react';
import {
  YMaps,
  Map,
  Placemark,
  ZoomControl,
  TypeSelector,
  // $flow-disable-line
} from 'react-yandex-maps';

import style from './style.scss';

type Props = {
  page: Object,
};

export default memo<Props>(({ page }: Props) => {
  const mapRef = useRef(null);

  const placeMark = {
    geometry: page.coordinates,
    properties: {
      hintContent: `Барбершоп Garage в ${page.hint}`,
    },
    modules: ['geoObject.addon.balloon', 'geoObject.addon.hint'],
  };

  return (
    <YMaps
      enterprise
      query={{
        apikey: '78c31454-b712-4e2b-83bc-a74e77c6d541',
        lang: 'ru_RU',
      }}
      modules={['smallMapDefaultSet']}
    >
      <div className={style.map_container}>
        <Map
          state={{
            center: page.coordinates,
            zoom: 17,
          }}
          className={style.map_wrapper}
          instanceRef={mapRef}
          options={{
            maxZoom: 18,
            minZoom: 6,
          }}
        >
          <Placemark {...placeMark} />
          <ZoomControl
            options={{
              position: {
                left: '10px',
                top: '10px',
              },
              size: 'small',
            }}
          />
          <TypeSelector
            options={{
              position: {
                right: '10px',
                top: '10px',
              },
              size: 'small',
            }}
          />
        </Map>
      </div>
    </YMaps>
  );
});
