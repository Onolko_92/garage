// @flow
import React from 'react';

import Follow from '@components/common/followButton';

import style from './style.scss';

type Props = {
  page: Object,
};

export default ({ page }: Props) => {
  return (
    <div className={style.stock} id="stock">
      <h1>Действует акция</h1>
      <span>{page.stock}</span>
      <Follow page={page} type="header" text="follow" />
    </div>
  );
};
