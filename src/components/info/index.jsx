// @flow
import React, { memo } from 'react';
import ADDRESS from '@public/address.jpg';
import PHONE from '@public/phone.jpg';
import WATCH from '@public/watch.jpg';

import style from './style.scss';

type Props = {
  page: Object,
};

export default memo<Props>(({ page }: Props) => {
  return (
    <div className={style.info}>
      <div className={style.info__container}>
        <img data-src={ADDRESS} alt={page.address} />
        <span>{page.city}</span>
        <span>{page.address}</span>
      </div>
      <div className={style.info__container}>
        <img data-src={WATCH} alt={page.address} />
        <span>Ежедневно</span>
        <span>{page.openW}</span>
      </div>
      <div className={style.info__container}>
        <img data-src={PHONE} alt={page.address} />
        <span>Телефон</span>
        <span>{page.phone}</span>
      </div>
    </div>
  );
});
