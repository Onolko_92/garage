// @flow
import React, { memo } from 'react';
// $flow-disable-line
import cn from 'classnames';

import style from './style.scss';

type Props = {
  page: Object,
};

const commonData = {
  width: '100%',
  height: '100%',
  backgroundPosition: 'center center',
  backgroundSize: 'cover',
  backgroundRepeat: 'no-repeat',
};

const masterElement = masters => {
  return masters.map(m => (
    <div key={m.link} className={style.master_element}>
      <div
        style={{
          backgroundImage: `url(${m.img})`,
          ...commonData,
        }}
        className="lazy"
      />
      <div className={style.master_element__name}>
        <span>{m.name}</span>
        <span>{m.jobs}</span>
      </div>
      <a href={m.link} target="_blank" rel="noopener noreferrer">
        Записаться
      </a>
    </div>
  ));
};

export default memo<Props>(({ page }: Props) => {
  const title = `Познакомьтесь с лучшими \n мастерами в ${page.hint}`;
  return (
    <div className={style.masters_block} id="masters">
      <div className={style.bg_wrap}>
        <div
          className={cn(style.background, 'lazy')}
          style={{
            background: `url(${page.mastersBackground}) center center no-repeat`,
          }}
        />
      </div>
      <div className={style.filter} />
      <h1>{title}</h1>
      <span>{page.mastersText}</span>
      <div className={style.master_wrapper}>{masterElement(page.masters)}</div>
    </div>
  );
});
