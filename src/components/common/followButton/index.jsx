// @flow
import React, { memo } from 'react';
// $flow-disable-line
import cn from 'classnames';

import lang from './translations';

import style from './style.scss';

type Props = {
  page: Object,
  type: string,
  text: string,
};

export default memo<Props>(({ page, type, text }: Props) => {
  const classes = cn(style.follow, style[type]);
  return (
    <div className={classes} data-name="follow-button">
      <a target="_blank" rel="noopener noreferrer" href={page.follow}>
        <span>{lang.ru[text]}</span>
      </a>
    </div>
  );
});
