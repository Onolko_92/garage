// @flow
import React, { memo } from 'react';
// $flow-disable-line
import { Helmet } from 'react-helmet';

type Props = {
  page: Object,
};

const MetaTags = ({ page }: Props) => {
  const { meta } = page;
  const openGraph = [
    {
      property: 'og:site_name',
      content: meta.title,
    },
    {
      property: 'og:type',
      content: 'website',
    },
    {
      property: 'og:url',
      content: meta.canonical,
    },
    {
      property: 'og:description',
      content: meta.description,
    },
    {
      property: 'og:image',
      content: meta.image,
    },
  ];

  return (
    <Helmet
      htmlAttributes={{ lang: 'ru' }}
      title={meta.title}
      meta={[
        {
          name: 'description',
          content: meta.description,
        },
        ...openGraph,
      ]}
      link={[{ rel: 'canonical', href: meta.canonical }]}
    />
  );
};

export default memo<Props>(MetaTags);
