/* eslint-disable no-param-reassign */
// @flow
import React, { memo, useEffect, useState } from 'react';
import type { Node } from 'react';

import Header from '@components/header';
import Footer from '@components/footer';
import Progress from '@components/progressBar';

import { calculateScrollDistance } from '@utils';

import style from './style.scss';

type Props = {
  children: Node,
  page: string,
  setPage: string => void,
};

export default memo<Props>(({ children, setPage, page }: Props) => {
  const [scrollPosition, setScrollPosition] = useState(0);

  useEffect(() => {
    document.addEventListener('scroll', () => {
      requestAnimationFrame(() => {
        calculateScrollDistance(setScrollPosition);
      });
    });
    return () => {
      document.removeEventListener('scroll', () => {
        requestAnimationFrame(() => {
          calculateScrollDistance(setScrollPosition);
        });
      });
    };
  }, []);

  useEffect(() => {
    [].forEach.call(document.querySelectorAll('img[data-src]'), img => {
      img.setAttribute('src', img.getAttribute('data-src'));
      // eslint-disable-next-line no-param-reassign
      img.onload = () => {
        img.removeAttribute('data-src');
      };
    });
  });

  useEffect(() => {
    let lazyloadImages;
    if ('IntersectionObserver' in window) {
      lazyloadImages = document.querySelectorAll('.lazy');
      const imageObserver = new IntersectionObserver(() => entries => {
        entries.forEach(entry => {
          if (entry.isIntersecting) {
            const image = entry.target;
            image.classList.remove('lazy');
            imageObserver.unobserve(image);
          }
        });
      });

      lazyloadImages.forEach(image => {
        imageObserver.observe(image);
      });
    } else {
      let lazyloadThrottleTimeout;
      lazyloadImages = document.querySelectorAll('.lazy');
      const lazyload = () => {
        if (lazyloadThrottleTimeout) {
          clearTimeout(lazyloadThrottleTimeout);
        }

        lazyloadThrottleTimeout = setTimeout(() => {
          const scrollTop = window.pageYOffset;
          lazyloadImages.forEach(img => {
            if (img.offsetTop < window.innerHeight + scrollTop) {
              // $flow-disable-line
              img.src = img.dataset.src;
              img.classList.remove('lazy');
            }
          });
          if (lazyloadImages.length === 0) {
            document.removeEventListener('scroll', lazyload);
            window.removeEventListener('resize', lazyload);
            window.removeEventListener('orientationChange', lazyload);
          }
        }, 20);
      };

      document.addEventListener('scroll', lazyload);
      window.addEventListener('resize', lazyload);
      window.addEventListener('orientationChange', lazyload);
    }
  });

  return (
    <div className={style.container}>
      <Progress scroll={scrollPosition} />
      <div className={style.header}>
        <Header setPage={setPage} page={page} />
      </div>
      <div className={style.children}>{children}</div>
      <div className={style.footer}>
        <Footer page={page} />
      </div>
    </div>
  );
});
