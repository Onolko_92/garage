FROM node:13-slim
RUN apt-get -qy update && apt-get -qy install openssl

ENV NODE_ENV=production

WORKDIR /app
COPY . ./
RUN yarn install --production=false --frozen-lockfile --non-interactive --ignore-scripts
RUN yarn build

EXPOSE 3001

CMD ["yarn", "start"]
