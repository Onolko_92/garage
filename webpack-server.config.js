/* eslint-disable import/no-extraneous-dependencies */
const path = require('path');
const nodeExternals = require('webpack-node-externals');

const webpackConfig = {
  entry: {
    server: './server/index.js',
  },
  output: {
    path: path.resolve(__dirname, './.next/dist'),
    filename: '[name].js',
  },
  mode: 'production',
  name: 'server',
  target: 'node',
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
    ],
  },
  externals: [nodeExternals()],
};

module.exports = webpackConfig;
