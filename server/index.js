/* eslint-disable no-console */
const Koa = require('koa');
const Next = require('next');
const Router = require('koa-router');
const { parse } = require('url');

const port = parseInt(process.env.PORT, 10) || 3001;
const dev = process.env.NODE_ENV === undefined;
const app = Next({ dev });

const handle = app.getRequestHandler();

app.prepare().then(() => {
  const server = new Koa();
  const router = new Router();

  router.all('*', async ctx => {
    const { req, res } = ctx;
    const parsedUrl = parse(req.url, true);

    await handle(req, res, parsedUrl);
    ctx.respond = false;
  });

  server.use(async (ctx, next) => {
    ctx.res.statusCode = 200;
    await next();
  });

  server.use(router.routes());

  server.on('error', err => {
    const date = new Date();
    const time = date.toISOString();
    console.error('Time --> ', time);
    console.log(err);
  });

  server
    .listen(port, () => {
      console.log(`> Ready on http://localhost:3001`);
    })
    .setTimeout(10000);
});
