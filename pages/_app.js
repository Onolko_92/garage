// @flow
import React, { useState, useEffect } from 'react';
import type { Node } from 'react';
import App from 'next/app';
import { Helmet } from 'react-helmet';
import cookies from 'js-cookie';
import cookie from 'cookie';

import 'swiper/css/swiper.min.css';

import * as Page from '@constants/contacts';

import { redirect } from '../pagesUtils';
import '../src/layout/style.scss';

import Layout from '../src/layout';

type Props = {
  Component: () => Node,
  pageProps: Object,
  page: Object,
};

const mogilev = Page.coordinatesMOG;
const minsk = Page.coordinatesMIN;

const getCurrentValue = (coords, city) => {
  return Math.abs(+city[0] - +coords.latitude + +city[1] - +coords.longitude);
};

const MyApp = ({ Component, pageProps, page }: Props) => {
  const [currentPage, setCurrentPage] = useState(page);

  const isClient = typeof window !== 'undefined';
  const cookiePage = isClient ? cookies.get('city') : false;

  const getNewPage = (cP, href, cV) => {
    setCurrentPage(cP);
    cookies.set('city', cV, {
      expires: 365,
      path: '/',
    });
    window.location.href = href;
  };

  useEffect(() => {
    if (isClient && !cookiePage) {
      navigator.geolocation.getCurrentPosition(({ coords }) => {
        const mogilevResult = getCurrentValue(coords, mogilev);
        const minskResult = getCurrentValue(coords, minsk);
        if (minskResult < mogilevResult) {
          getNewPage(Page.minsk, '/minsk', 'minsk');
        } else {
          getNewPage(Page.mogilev, '/', 'mogilev');
        }
      });
    }
  }, [cookiePage, currentPage, isClient, page, page.id]);

  return (
    <Layout page={page} setPage={setCurrentPage}>
      <Helmet htmlAttributes={{ lang: 'ru' }} />
      <Component {...pageProps} page={currentPage} setPage={setCurrentPage} />
    </Layout>
  );
};

MyApp.getInitialProps = async appContext => {
  const appProps = await App.getInitialProps(appContext);
  const { asPath, req, res } = appContext?.ctx || {};
  let page = Page.mogilev;
  const cookieStr = req?.headers?.cookie;
  const { city } = cookie.parse(cookieStr || '');
  if (asPath === '/minsk') {
    page = Page.minsk;
  }
  if (asPath !== '/minsk' && city === 'minsk') {
    redirect(res, 302, '/minsk');
  }
  if (asPath !== '/' && city === 'mogilev') {
    redirect(res, 302, '/');
  }
  return { ...appProps, page };
};

export default MyApp;
