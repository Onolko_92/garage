// @flow
import React from 'react';

class Error extends React.PureComponent<Props> {
  static async getInitialProps(ctx) {
    const { res, asPath, err } = ctx;

    let statusCode = 404;
    if (res) {
      statusCode = res.statusCode || 404;
    } else {
      statusCode = err ? err.statusCode : 404;
    }
    // eslint-disable-next-line no-console
    console.log(`error->>>${statusCode} ${err}`);
    return { statusCode, asPath };
  }

  render() {
    return 'UPS';
  }
}

export default Error;
