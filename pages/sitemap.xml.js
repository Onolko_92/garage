import { Component } from 'react';

const sitemapXml = `<?xml version="1.0" encoding="UTF-8"?>
<urlset 
  xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" 
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
  xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 
  http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
	  <url>
		  <loc>http://garagebarbershop.by/</loc>
      <lastmod>2020-06-24T10:06:29+00:00</lastmod>
    </url>
    <url>
      <loc>http://garagebarbershop.by/minsk</loc>
      <lastmod>2020-06-24T10:06:29+00:00</lastmod>
      <priority>0.80</priority>
    </url>
</urlset>`;

export default class Sitemap extends Component {
  static getInitialProps({ res }) {
    res.setHeader('Content-Type', 'text/xml');
    res.write(sitemapXml);
    res.end();
  }
}
