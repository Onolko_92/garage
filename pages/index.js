// @flow
import React from 'react';
import App from '../src/components/page';

type Props = {
  page: Object,
};

export default (props: Props) => {
  return <App {...props} />;
};
