import { Component } from 'react';

const robotsTxt = `
  User-Agent: *
  Sitemap: http://garagebarbershop.by/sitemap.xml
  Host: garagebarbershop.by
`;

export default class Robots extends Component {
  static getInitialProps({ res }) {
    res.setHeader('Content-Type', 'text/plain');
    res.write(robotsTxt);
    res.end();
  }
}
