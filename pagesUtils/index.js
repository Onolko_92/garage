export const redirect = (res, statusCode, url) => {
  res.writeHead(statusCode, {
    Location: url,
  });
  res.end();
};
