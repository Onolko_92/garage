module.exports = ({ cache }) => {
  const env = process.env.BABEL_ENV;
  cache(() => env);

  return env === 'server'
    ? {
        presets: [
          [
            'next/babel',
            {
              'preset-env': {
                targets: {
                  node: 'current',
                },
              },
              'transform-runtime': {
                useESModules: false,
              },
            },
          ],
        ],
      }
    : {
        presets: ['next/babel'],
        plugins: [
          '@babel/plugin-transform-flow-strip-types',
          '@babel/plugin-proposal-optional-chaining',
          '@babel/plugin-proposal-export-default-from',
          [
            '@babel/plugin-transform-react-jsx',
            {
              pragmaFrag: 'React.Fragment',
            },
          ],
        ],
      };
};
